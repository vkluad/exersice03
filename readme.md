# [Exercise #3. Git Branching and Merging](https://gl-khpi.gitlab.io/exercises/exercise03/)

## Description

Work with branches in the local repository.

---

## References

- [Git Branching.](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
- [Git Tutorials - Using Branches.](https://www.atlassian.com/git/tutorials/using-branches)

---

## Requirements
Project directory name: `exercise03`

---

## Guidance
1. Setup name, email and line ending preferences
- If you have never used git before, you need to do some setup first.
- If you have git already setup, you can proceed to the next step.

```sh
git config --global user.name "Name Surname"
git config --global user.email "mailbox@example.com"

# Also, for Unix/Mac users:
git config --global core.autocrlf input
git config --global core.safecrlf true

# And for Windows users:
git config --global core.autocrlf true
git config --global core.safecrlf true
```
2. Initialize a repository
- Create a new directory.
- Initialize a new repository in the created directory.

```sh
mkdir exercise03
cd exercise03
git init
```

3. Create a project with an empty file `readme.md`
- Create a project directory.
- Add `readme.md`.
- Commit all changes.

```sh
mkdir pro
cd pro
touch readme.md
git add .
git commit -m "repo: initial commit"
```

4. Create a new branch and modify `readme.md`
- Create a new branch named `first_branch` and switch to it.
- Modify the `readme.md`: add a list of commands to solve the **1st** subtask (1st step).
- Display the state of the working directory and the staging area (list which files are staged, unstaged, and untracked).
- Commit all changes.

```sh
git checkout -b first_branch
gedit readme.md 
git status
git commit -am "readme: add the command log of the 1st subtask"
```

5. Switch back to the master branch and modify `readme.md`
- Switch back to the master branch.
- Modify the `readme.md`: add a list of commands to solve the **2nd** subtask.
- Commit all changes.
- Display the project history.

```sh
git checkout master
gedit readme.md
git commit -am "readme: add command log to solve 2nd subtask"
git log --oneline --decorate --graph --all
```

6. Merge the first_branch back into your master branch
- Show the working tree status.
- Merge the changes made to the `first_branch` on top of master
- Show the working tree status.
- Display the project history.

```sh
git status
git merge first_branch 
git mergetool
git status
git log --oneline --decorate --graph --all
```

7. Continue editing your readme.md
- Modify the `readme.md`: add a list of commands to solve **3rd** subtask.
- Commit all changes.
- Add commands for solving the remaining subtasks to the `readme.md` in separate commits.
- Display the project history.

```sh
gedit readme.md
git commit -am "readme: add command log to solve 3rd subtask"
gedit readme.md
git commit -am "readme: add command log to solve 4th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 5th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 6th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 7th subtask"
git log --oneline --decorate --graph --all
```
---
